# cv -- my curriculum vitae
- - -

## Table of Contents
1. Introduction
2. Dependencies
3. Usage
4. Development

- - -
## 1. Introduction 

This repo contains my resume. As my academic career progress, it will evolve
into a curriculum vitae, CV.

## 2. Dependencies

* [latex](http://www.latex-project.org)

## 3. Usage

### To build 

	pdflatex resume_martin_velez.tex

### To clean generated files
 
	rake clean

### To clean ALL generated files (including PDF)

	rake clobber

## 4. Development
* Author: 					[Martin Velez](http://www.martinvelez.com)
* Copyright: 				Copyright (C) 2011 [Martin Velez](http://www.martinvelez.com)
* License: 					[GPL](http://www.gnu.org/copyleft/gpl.html)

### Source 
[Bitbucket](https://bitbucket.org/martinvelez/cv) is hosting this code.
	
	https://bitbucket.org/martinvelez/cv

